$(document).ready(function(){
    
    function init(){
        
        $("form input.stored").each(function(){
        var form = $(this).parent();
        var formId = form.attr("id");
        var dataType = $(this).data("type");
        
        if(localStorage.getItem("#"+formId+"input[data-type='"+dataType+"']"))
            $(this).val(localStorage.getItem("#"+formId+"input[data-type='"+dataType+"']"));
        });
    }
    
    init();
    
    $("input.stored").keyup(function(){
        var form = $(this).parent();
        var formId = form.attr("id");
        var dataType = $(this).data("type");
        localStorage.setItem("#"+formId+"input[data-type='"+dataType+"']", $(this).val());
    });
    
    $("form input[type='submit']").click(function(event){
        $("input.stored", event.target.parentNode ).each(function(){
        var form = $(this).parent();
        var formId = form.attr("id");
        var dataType = $(this).data("type");
        localStorage.removeItem("#"+formId+"input[data-type='"+dataType+"']", $(this).val());
        $("#"+formId+"input[data-type='"+dataType+"']").val("");
        });
    });
});